t = gets.to_i

1.upto(t) do |i|
  values = gets.split(' ').map(&:to_i)
  f = values.first
  s = values.last

  tickets = []

  1.upto(f) do
    tickets << gets.split(' ').map(&:to_i)
  end

  # get rid of duplicates
  tickets.uniq!

  # array index is row number
  row_counts = Array.new(s + 1, 0)

  tickets.each do |a, b|
    if a == b
      row_counts[a] += 1
    else
      row_counts[a] += 1
      row_counts[b] += 1
    end
  end

  puts "Case ##{i}: #{row_counts.max}"
end
